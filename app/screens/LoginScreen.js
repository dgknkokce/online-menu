import React from 'react';
import { ImageBackground, StyleSheet, View, Image, Text, Button, SafeAreaView, Alert, TextInput} from "react-native";
import CustomButton from '../components/CustomButton';

function LoginScreen(props) {
    return (
        <ImageBackground
            style={styles.background}  
            source={require('../assets/background.jpg')}
        >
            <View style={styles.logoContainer}>
                <Image style={styles.logo} source={require('../assets/hamburger.png')} />
                <Text style={styles.text}>Ne Söylesem</Text>
                <Text style={styles.text}>Online Menu System</Text>
            </View>


        <View style={styles.container}>
                <TextInput
                value=""
                placeholder={'Username'}
                style={styles.input}
                />
                <TextInput
                value=""
                placeholder={'Password'}
                style={styles.input}
                />
                
                <CustomButton text="Login" onPress={props.handleLogin}/>
            </View>
        </ImageBackground>
    );
}


const styles = StyleSheet.create({
    background: {
        flex: 1,
        justifyContent: "flex-end",
        alignItems: "center"
    },
    loginButton: {
        width: '100%',
        height: 70,
        backgroundColor: '#fc5c65'
    },
    ScanMenuButton: {
        width: '100%',
        height: 70,
        backgroundColor: '#4ecdc4'
    },
    logo: {
        width: 100,
        height: 100,
    },
    logoContainer: {
        position: 'absolute',
        top: 70,
        alignItems: 'center'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ecf0f1',
        position: 'absolute'
    },
    input: {
        width: 200,
        height: 44,
        padding: 10,
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 10,
    },
    text: {
        color: 'white'
    }
})
export default LoginScreen;